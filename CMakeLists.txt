cmake_minimum_required(VERSION 3.10)
project(LevelEditor)

add_executable (${PROJECT_NAME}
    main.cpp
    leConfig.cpp
    leConfig.hpp
    leProgram.cpp
    leProgram.hpp
    leScene.cpp
    leScene.hpp
)

SET_TARGET_PROPERTIES (${PROJECT_NAME} PROPERTIES CXX_STANDARD 17 LINKER_LANGUAGE CXX)

IF (WIN32)
    SET (${PROJECT_NAME}_PLATFORM_SPECIFIC_INCLUDE_DIRS
        ${CMAKE_CURRENT_LIST_DIR}/../KtnEngine/external/glfw/include
        ${CMAKE_CURRENT_LIST_DIR}/../KtnEngine/external/glm
        ${CMAKE_CURRENT_LIST_DIR}/../KtnEngine/external/libepoxy/include
        ${CMAKE_CURRENT_LIST_DIR}/../KtnEngine/external/ogg/include
        ${CMAKE_CURRENT_LIST_DIR}/../KtnEngine/external/portaudio/include
        ${CMAKE_CURRENT_LIST_DIR}/../KtnEngine/external/vorbis/include
        C:\\VulkanSDK\\1.1.114.0\\Include
    )

    SET (${PROJECT_NAME}_PLATFORM_SPECIFIC_LIBS
        ${CMAKE_CURRENT_LIST_DIR}/../KtnEngine/external/glfw/build/src/glfw3dll.lib
        ${CMAKE_CURRENT_LIST_DIR}/../KtnEngine/external/libepoxy/lib/epoxy.lib
        ${CMAKE_CURRENT_LIST_DIR}/../KtnEngine/external/portaudio/build/Release/portaudio_x64.lib
        ${CMAKE_CURRENT_LIST_DIR}/../KtnEngine/external/vorbis/build/lib/vorbisfile.lib
        C:\\VulkanSDK\\1.1.114.0\\Lib\\vulkan-1.lib
        opengl32.lib
    )
ENDIF (WIN32)

TARGET_INCLUDE_DIRECTORIES (${PROJECT_NAME} PRIVATE ${ktnEngine_INCLUDE_DIR} ${${PROJECT_NAME}_PLATFORM_SPECIFIC_INCLUDE_DIRS})
TARGET_LINK_LIBRARIES (${PROJECT_NAME} ktnEngine ${${PROJECT_NAME}_PLATFORM_SPECIFIC_LIBS})

FILE (COPY ${CMAKE_CURRENT_LIST_DIR}/../resources DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

IF (WIN32)
    FILE (COPY
        ${CMAKE_CURRENT_SOURCE_DIR}/../KtnEngine/external/glfw/build/src/glfw3.dll
        ${CMAKE_CURRENT_SOURCE_DIR}/../KtnEngine/external/libepoxy/bin/epoxy-0.dll
        ${CMAKE_CURRENT_SOURCE_DIR}/../KtnEngine/external/ogg/build/ogg.dll
        ${CMAKE_CURRENT_SOURCE_DIR}/../KtnEngine/external/portaudio/build/Release/portaudio_x64.dll
        ${CMAKE_CURRENT_SOURCE_DIR}/../KtnEngine/external/vorbis/build/lib/vorbis.dll
        ${CMAKE_CURRENT_SOURCE_DIR}/../KtnEngine/external/vorbis/build/lib/vorbisfile.dll

        DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
    )
ENDIF (WIN32)

