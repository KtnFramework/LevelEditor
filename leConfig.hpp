#ifndef LECONFIG_H
#define LECONFIG_H
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

class leConfig {
public:
    leConfig();
    rapidjson::Document _content;
};

#endif // LECONFIG_H
