#include <ktnEngine/ktnGraphics>

#include "leProgram.hpp"
#include "leScene.hpp"

#include <glm/gtc/matrix_transform.hpp>

#include <mutex>
#include <random>
#include <thread>

using namespace glm;
using namespace ktn;
using namespace ktn::Application;
using namespace ktn::Graphics;
using namespace ktn::Physics;
using namespace std;

int main() {
    leProgram p;
    auto graphicsScene = make_shared<leScene>();
    auto renderer = make_shared<ktn::Graphics::ktnRenderer_OpenGL>();
    ktnGame game(vk::Extent2D(1280, 720), renderer);

    game.WindowName = "Model Viewer";
    game.Initialize();

    // This is particularly leaking memory.
    // If we create normal variables here they will go out of scope after the if
    // We keep it for the time being, as the vulkan backend needs to have the
    // shader and model classes changed as well.
    if (game.GraphicsBackend == ktnGraphicsBackend::OPENGL) {

        auto robot = make_shared<ktnModel>("resources/models/placeholder_robot.gltf");

        graphicsScene->StartAction.Connect(robot.get(), &ktnModel::StartAction);
        for (auto &[index, action] : robot.get()->Actions) { //
            action->Completed.Connect(graphicsScene.get(), &leScene::HandleActionComplete);
        }

        robot->Name = "Model";
        graphicsScene->AddModel(robot);

        // set up frame buffer
        uint ShadowMapSize = 512;
        ktnFrameBuffer *shadowbuffer = new ktnFrameBuffer(FrameBufferType::DEPTH, vk::Extent2D(ShadowMapSize, ShadowMapSize));

        // add the buffers to the scene
        graphicsScene->ShadowBuffer_Directional.emplace_back(shadowbuffer);
    }
    ktnScene *scene = new ktnScene(graphicsScene, make_shared<ktnPhysicsScene>());

    game.SetActiveScene(scene);

    // render loop
    game.Run();

    return 0;
}
