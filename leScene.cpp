#include "leScene.hpp"

#include <thread>

using namespace glm;
using namespace ktn;
using namespace Graphics;
using namespace std;

leScene::leScene() {
    AddCamera(ktnCamera());
    SetCurrentCamera(Cameras[0].Name.ToStdString());

    CurrentCamera()->SetType(CameraType::FIRSTPERSON);

    CurrentCamera()->SetMode(CameraMode::TRACKING);
    CurrentCamera()->SetPosition({-2.f, 1.0f, 2.0f}, WorldSpace);
    CurrentCamera()->TurnLeft(450);
    CurrentCamera()->TurnDown(150);
    CurrentCamera()->SetMode(CameraMode::FREELOOK);

    srand(static_cast<uint32_t>(chrono::steady_clock::now().time_since_epoch().count()));

    ktnLight light(string("light").c_str());

    light.Intensity = 1000;
    light.Falloff.LinearTerm = 0.5f;
    light.Falloff.QuadraticTerm = 0.5f;
    AddLight(light);

    Lights[0].SetPosition(CurrentCamera()->Position(WorldSpace) + CurrentCamera()->Front() * 0.1f, WorldSpace);
    Lights[0].SetDirection(CurrentCamera()->Front());

    for (int i = 0; i < 10; ++i) {
        ktn::Graphics::ktnLight lightIteration((string("light") + to_string(i)).c_str());
        lightIteration.Intensity = 10.0f * (rand() % 10);
        lightIteration.SetPosition(vec3(0.2f * (rand() % 100 - 50), 0.1f * (rand() % 100 - 50), 0.1f * (rand() % 100)), WorldSpace);
        lightIteration.SetColor(0.01f * (rand() % 100), 0.01f * (rand() % 100), 0.01f * (rand() % 100));
        lightIteration.Falloff.LinearTerm = 0.5f;
        lightIteration.Falloff.QuadraticTerm = 0.5f;
        AddLight(lightIteration);
    }
    m_InputMapper.Initialize();
}

void leScene::ProcessInput() {
    auto currentFrameTime = static_cast<float>(glfwGetTime());
    m_DeltaTime = currentFrameTime - m_LastFrameTime;
    m_LastFrameTime = currentFrameTime;
    float cameraSpeed = 2.0f * m_DeltaTime;

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_ESCAPE]) == GLFW_PRESS) { glfwSetWindowShouldClose(m_Window, 1); }

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_W]) == GLFW_PRESS) { CurrentCamera()->MoveForward(cameraSpeed); }

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_A]) == GLFW_PRESS) { CurrentCamera()->MoveLeft(cameraSpeed); }

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_S]) == GLFW_PRESS) { CurrentCamera()->MoveBackward(cameraSpeed); }

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_D]) == GLFW_PRESS) { CurrentCamera()->MoveRight(cameraSpeed); }

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_UP]) == GLFW_PRESS) {
        CurrentCamera()->TurnUp(1);
        CurrentCamera()->UpdateFront();
        CurrentCamera()->UpdateViewMatrix();
    }

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_DOWN]) == GLFW_PRESS) {
        CurrentCamera()->TurnDown(1);
        CurrentCamera()->UpdateFront();
        CurrentCamera()->UpdateViewMatrix();
    }

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_LEFT]) == GLFW_PRESS) {
        CurrentCamera()->TurnLeft(1);
        CurrentCamera()->UpdateFront();
        CurrentCamera()->UpdateViewMatrix();
    }

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_RIGHT]) == GLFW_PRESS) {
        CurrentCamera()->TurnRight(1);
        CurrentCamera()->UpdateFront();
        CurrentCamera()->UpdateViewMatrix();
    }

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_Q]) == GLFW_PRESS) { HandleQPress(); }
    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_E]) == GLFW_PRESS) { HandleEPress(); }

    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_LEFT_CONTROL]) == GLFW_PRESS) { HandleCtrlPress(); }
    if (glfwGetKey(m_Window, m_InputMapper.Map[KTNKEY_SPACE]) == GLFW_PRESS) { HandleSpacePress(); }

    CurrentCamera()->UpdateViewMatrix();

    if (m_LeftMouseIsClicking) {
        Lights[0].SetPosition(CurrentCamera()->Position(WorldSpace) + CurrentCamera()->Front() * 0.1f, WorldSpace);
        Lights[0].SetDirection(CurrentCamera()->Front());
    }
}

void leScene::HandleEvent_ItemsDroppedIntoWindow(GLFWwindow *window, int count, const char **paths) {
    for (int i = 0; i < count; ++i) {
        try {
            auto model = make_shared<ktnModel>(paths[i]);
            AddModel(model);
        } catch (std::exception &e) { cout << e.what() << endl; }
    }
}

void leScene::HandleEvent_MouseClicked(GLFWwindow *window, int button, int action, int mods) {
    // for the time being the modification does nothing, but can be added to do stuff later.
    if (mods != 0) {}

    if ((window != nullptr) && GLFW_MOUSE_BUTTON_LEFT == button) {
        if (action == GLFW_PRESS) { m_LeftMouseIsClicking = true; }
        if (action == GLFW_RELEASE) {
            m_LeftMouseIsClicking = false;
            if (m_LeftMouseDragged) { // select object if mouse was not dragged?
                m_LeftMouseDragged = false;
            }
        }
    }
}

void leScene::HandleEvent_MouseMoved(GLFWwindow *window, double xpos, double ypos) {
    if (window != nullptr) {
        if (m_FirstMouse) {
            m_LastX = xpos;
            m_LastY = ypos;
            m_FirstMouse = false;
        }

        if (m_LeftMouseIsClicking) {
            m_LeftMouseDragged = true;
            CurrentCamera()->TurnRight(float(xpos - m_LastX));
            CurrentCamera()->TurnDown(float(ypos - m_LastY));
            CurrentCamera()->UpdateFront();
            CurrentCamera()->UpdateViewMatrix();
        }

        m_LastX = xpos;
        m_LastY = ypos;
    }
}

void leScene::HandleActionComplete(ktn::Graphics::ktnAction *eAction) {
    //    StartAction(eAction->Name);
}

void leScene::HandleQPress() {
    IncrementJointNr();
}

void leScene::HandleEPress() {
    DecrementJointNr();
}

void leScene::HandleCtrlPress() {
    m_Models.at(0)->StartAction("Idle");
    //    Models.at(0)->Skins.at(0)->Joints.at(_currentJoint).second->OffsetPosition({0, -0.1, 0}, LocalSpace);
}

void leScene::HandleSpacePress() {
    m_Models.at(0)->StartAction("walk");
    //    Models.at(0)->Skins.at(0)->Joints.at(_currentJoint).second->OffsetPosition({0, 0.1, 0}, LocalSpace);
}

void leScene::IncrementJointNr() {
    //    ++_currentJoint;
    //    cout << _currentJoint << endl;
    //    this_thread::sleep_for(chrono::milliseconds(100));
    //    _currentJoint = std::clamp(_currentJoint, 0, 4);
}

void leScene::DecrementJointNr() {
    //    --_currentJoint;
    //    cout << _currentJoint << endl;
    //    this_thread::sleep_for(chrono::milliseconds(100));
    //    _currentJoint = std::clamp(_currentJoint, 0, 4);
}
