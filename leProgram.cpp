#include "leProgram.hpp"

#include <filesystem>
#include <fstream>
#include <iostream>

using namespace std;

leProgram::leProgram() {
    auto configdir = filesystem::path(getenv("HOME")) / ".config" / "KtnEngine Level Editor";
    leConfig f;
    if (!filesystem::exists(configdir)) { filesystem::create_directory(configdir); }
}
