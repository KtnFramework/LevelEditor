#ifndef LEVELEDITOR_LESCENE_HPP
#define LEVELEDITOR_LESCENE_HPP
#include <ktnEngine/ktnApplication>
#include <ktnSignalSlot/ktnSignalSlot.hpp>

class leScene : public ktn::Graphics::ktnGraphicsScene {
public:
    leScene();
    void ProcessInput() final;

    ktn::ktnInputMapper m_InputMapper;

    ktnSignal<std::string> StartAction;
    ktnSignal<> SpaceIsPressed;
    ktnSignal<> CtrlIsPressed;

    void HandleActionComplete(ktn::Graphics::ktnAction *);

protected:
    void HandleEvent_ItemsDroppedIntoWindow(GLFWwindow *window, int count, const char **paths) final;
    void HandleEvent_MouseClicked(GLFWwindow *window, int button, int action, int mods) final;
    void HandleEvent_MouseMoved(GLFWwindow *window, double xpos, double ypos) final;

private:
    void HandleQPress();
    void HandleEPress();

    void HandleCtrlPress();
    void HandleSpacePress();

    void IncrementJointNr();
    void DecrementJointNr();
    int _currentJoint = 0;

    bool m_LeftMouseDragged = false;
};

#endif // LEVELEDITOR_LESCENE_HPP
