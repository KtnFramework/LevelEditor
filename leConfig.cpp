#include "leConfig.hpp"

#include <iostream>

using namespace std;
using namespace rapidjson;

leConfig::leConfig() {
    _content.SetObject();
    _content.AddMember("mlem", "blep", _content.GetAllocator());

    StringBuffer b;
    Writer<StringBuffer> w(b);

    _content.Accept(w);

    cout << b.GetString() << endl;
}
